<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>org.ssldev</groupId>
	<artifactId>serato-crate-exporter</artifactId>
	<version>0.3.3</version>
	<name>Serato Crate Exporter</name>
	<description>A simple java utility to select and export a Serato crate file into various file formats.</description>
	<url>https://gitlab.com/eladmaz/SeratoCrateExporter</url>

	<developers>
		<developer>
			<id>djelad</id>
			<email>djelad00@gmail.com</email>
			<organizationUrl>http://projects.ssldev.org</organizationUrl>
		</developer>
	</developers>

	<scm>
		<url>https://gitlab.com/eladmaz/SeratoCrateExporter.git</url>
	</scm>

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<junit.jupiter.version>5.5.2</junit.jupiter.version>
		<junit.platform.version>1.5.2</junit.platform.version>
		<javafx.version>11.0.2</javafx.version>
		<jpackager.path>/Library/Java/JavaVirtualMachines/openjdk-11.0.2.jdk/Contents/Home/bin/jpackager</jpackager.path>
	</properties>

	<repositories>
		<!-- <repository <id>jaudiotagger-repository</id> <url>https://dl.bintray.com/ijabz/maven</url> 
			</repository> -->
		<repository>
			<id>jitpack.io</id>
			<url>https://jitpack.io</url>
		</repository>
	</repositories>

	<build>
		<sourceDirectory>src/main/java</sourceDirectory>
		<plugins>
			<!-- used to copy win javafx classes into fat jar (hack to avoid app not running on windows machines) -->
			<plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<version>3.2.0</version>
				<executions>
					<execution>
						<id>copy-resources</id>
						<phase>validate</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${basedir}/target/classes</outputDirectory>
							<resources>
								<resource>
									<directory>src/non-packaged-resources</directory>
									<filtering>false</filtering>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.1</version>
				<configuration>
					<release>11</release>
				</configuration>
			</plugin>

			<!-- can be used to execute the API via mvn exec:java -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<version>1.6.0</version>
				<executions>
					<execution>
						<goals>
							<goal>exec</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<mainClass>org.ssldev.crateexporter.CrateExporterMain</mainClass>
					<executable>maven</executable>
				</configuration>
			</plugin>

			<!-- assembles all resources & dependencies into executable jar -->
			<!-- (e.g. mvn package) -->
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<archive>
						<manifest>
							<mainClass>org.ssldev.crateexporter.CrateExporterMain</mainClass>
							<addClasspath>true</addClasspath>
						</manifest>
					</archive>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
				</configuration>
			</plugin>

			<!-- javadoc & source generators -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>3.2.1</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<phase>deploy</phase>
						<goals>
							<goal>jar-no-fork</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.1.1</version>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<phase>deploy</phase>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- gpg signage -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-gpg-plugin</artifactId>
				<version>1.6</version>
				<executions>
					<execution>
						<id>sign-artifacts</id>
						<phase>verify</phase>
						<goals>
							<goal>sign</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<dependencies>
		<!-- SSL-API & event framework services dependencies -->
		<dependency>
			<groupId>org.ssldev</groupId>
			<artifactId>core</artifactId>
			<version>0.4-SNAPSHOT</version>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.ssldev</groupId>
			<artifactId>SSL-API</artifactId>
			<version>0.4-SNAPSHOT</version>
			<scope>compile</scope>
		</dependency>
		<!-- GUI elements -->
		<dependency>
			<groupId>org.openjfx</groupId>
			<artifactId>javafx-controls</artifactId>
			<version>${javafx.version}</version>
		</dependency>
		<dependency>
			<groupId>org.openjfx</groupId>
			<artifactId>javafx-graphics</artifactId>
			<version>${javafx.version}</version>
		</dependency>

		<dependency>
			<groupId>org.openjfx</groupId>
			<artifactId>javafx-media</artifactId>
			<version>${javafx.version}</version>
		</dependency>
		<dependency>
			<groupId>org.openjfx</groupId>
			<artifactId>javafx-web</artifactId>
			<version>${javafx.version}</version>
		</dependency>

		<!-- used to extract info from mp3 files -->
		<!-- note: built locally until maven repo can be updated -->
		<dependency>
			<groupId>net.jthink</groupId>
			<artifactId>jaudiotagger</artifactId>
			<version>2.2.6-SNAPSHOT</version>
			<!-- <groupId>com.github.goxr3plus</groupId> -->
			<!-- <version>2.2.7</version> -->
			<!-- <version>2.2.5</version> -->
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<version>${junit.jupiter.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-runner</artifactId>
			<version>${junit.platform.version}</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

</project>
