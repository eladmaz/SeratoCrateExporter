module org.ssldev.SeratoCrateExporter {
	exports org.ssldev.crateexporter.gui;
	
	opens org.ssldev.crateexporter.gui  to javafx.fxml;
	
	requires java.desktop;
	requires javafx.base;
	requires transitive javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires org.ssldev.api.sslapi;
	requires org.ssldev.core;
	requires jaudiotagger;  //TODO fix when jaudiotagger adds module-info
	
	
	//possible FIX for windows javafx dll issue:  "Graphics Device initialization failed for : d3d, sw" 
	requires javafx.media;
	requires javafx.web;
	
}