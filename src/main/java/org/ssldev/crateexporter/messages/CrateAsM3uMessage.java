package org.ssldev.crateexporter.messages;

import java.util.List;

import org.ssldev.core.messages.Message;
import org.ssldev.crateexporter.utils.CrateAudioFile;

public class CrateAsM3uMessage extends Message {

	public final String crateName;
	public final String contentAsM3u;
	public final List<CrateAudioFile> mp3s;

	public CrateAsM3uMessage(String crateName, String contentAsM3u, List<CrateAudioFile> mp3s) {
		this.crateName = crateName;
		this.contentAsM3u = contentAsM3u;
		this.mp3s = mp3s;
	}

}
