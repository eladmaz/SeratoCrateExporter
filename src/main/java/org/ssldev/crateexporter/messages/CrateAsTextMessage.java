package org.ssldev.crateexporter.messages;

import java.io.File;
import java.util.List;

import org.ssldev.core.messages.Message;
import org.ssldev.crateexporter.utils.CrateAudioFile;

/**
 * published Crate name and its contents in string format
 */
public class CrateAsTextMessage extends Message {
	public File crateFile;
	public String crateName;  // e.g example.crate
	public String content;
	public List<CrateAudioFile> mp3s;

	public CrateAsTextMessage(File crateFile, String crateName, String content, List<CrateAudioFile> mp3s) {
		this.crateFile = crateFile;
		this.crateName = crateName;
		this.content = content;
		this.mp3s = mp3s;
	}
	
	@Override
	public String toString() {
		return super.toString() + crateName;
	}

}
