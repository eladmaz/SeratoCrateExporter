package org.ssldev.crateexporter.messages;

import java.io.File;
import java.util.Objects;

import org.ssldev.core.messages.Message;
import org.ssldev.core.utils.Validate;
//import org.ssldev.core.utils.Validate;

/**
 * a request to convert the given crate file into a text file
 */
public class CrateToTextMessage extends Message{
	public File crateFile;
	public File txtFileOutput;
	public String crateName;

	public CrateToTextMessage(File crateFile, String crateName, File txtFileOutput) {
		Objects.requireNonNull(crateFile);
		Validate.isTrue(crateFile.exists(), "given crate file does not exist");
		
		this.crateFile = crateFile;
		this.crateName = crateName;
		this.txtFileOutput = txtFileOutput;
	}
	
	@Override
	public String toString() {
		return super.toString() + "crate file "+ crateFile;
	}
}
