package org.ssldev.crateexporter;

import java.util.logging.LogManager;

import org.ssldev.api.services.BytesToCrateConverterService;
import org.ssldev.api.services.CrateConvertService;
import org.ssldev.core.mgmt.AsyncEventHub;
import org.ssldev.core.services.FileWriterService;
import org.ssldev.core.services.SystemInfoValidatorService;
import org.ssldev.crateexporter.services.CrateExporterGuiService;
import org.ssldev.crateexporter.services.CrateToTextService;

public class CrateExporterMain {
	public static final String VERSION = "v0.3.3";
	private static AsyncEventHub hub;

	public static void main(String[] args) {
		hub = new AsyncEventHub(3);
		
		registerAllServices(hub);
		
		// attempt to quiet down the jaudiotagger logs
		LogManager.getLogManager().reset();
		
		hub.init();
		hub.start();
	}

	@SuppressWarnings("unused")
	private static void registerAllServices(AsyncEventHub hub) {

		// CORE services:
		SystemInfoValidatorService sysInfoService = new SystemInfoValidatorService(hub);
		FileWriterService writerService = new FileWriterService(hub);
		
		
		// SSL-API services:
		CrateConvertService crateConvertService = new CrateConvertService(hub); 
		BytesToCrateConverterService dataConsumer = new BytesToCrateConverterService(hub);
		
		// Crate Exporter services:
		CrateToTextService crateToTextService = new CrateToTextService(hub);
		CrateExporterGuiService gui = new CrateExporterGuiService(hub);
//		CrateToM3uService crateToM3uService = new  CrateToM3uService(hub);
		
		
	}
	
	public static void shutdown() {
		hub.shutdown();
	}
}
