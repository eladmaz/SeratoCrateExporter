package org.ssldev.crateexporter.gui;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.utils.Logger;
import org.ssldev.crateexporter.CrateExporterMain;
import org.ssldev.crateexporter.utils.CrateAudioFile;

import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
/**
 * GUI displays track data published, mostly for demo purposes.  
 */
public class CrateExporterGUI extends Application {
	
	private static CrateExporterMainController controller;
	private static List<Runnable> eventListeners = new LinkedList<>();
	Stage stage;
	Scene scene;
	static EventHub hub;
	private FXMLLoader rootLoader;
	private BorderPane rootPane;
	
	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		try {
			rootLoader = new FXMLLoader(getClass().getResource("/CrateExporterMain.fxml"));
			rootPane = rootLoader.load();
			controller = rootLoader.getController();
//			controller.init(this);
			controller.setGui(this);
			
			scene = new Scene(rootPane);
//			scene.getStylesheets().add(getClass().getResource("/demoGui.css").toExternalForm());
			scene.setFill(Color.TRANSPARENT);
			stage.setScene(scene);
			stage.setTitle("Crate Exporter " + CrateExporterMain.VERSION);
			stage.initStyle(StageStyle.UNIFIED);
			
			stage.show();
			
		} catch (IOException e) {
			Logger.error(this, e.getMessage());
			e.printStackTrace();
			stop();
		}
	}

	
	public static boolean isInitialized() {
		return controller != null;
	}
	

	public static void addListener(Runnable listener) {
		eventListeners.add(listener);
	}

	
	protected static class TrackGui{
		private final SimpleStringProperty key;
		private final SimpleIntegerProperty num;
		private final SimpleStringProperty artist;
		private final SimpleStringProperty title;
		private final SimpleStringProperty album;
		boolean errorEncounteredOnCreate;
		
		protected TrackGui(CrateAudioFile m, int _num) {
			if(m.encounteredErrorOnCreate()) {
				title = new SimpleStringProperty(m.getF().getName());
				artist = new SimpleStringProperty();
				key = new SimpleStringProperty();
				num = new SimpleIntegerProperty(_num);
				album = new SimpleStringProperty();
				errorEncounteredOnCreate = true;
			}
			else {
				
				title = new SimpleStringProperty (m.getTitle());
				artist = new SimpleStringProperty(m.getArtist());
				key = new SimpleStringProperty (m.getKey());
				num = new SimpleIntegerProperty(_num);
				album = new SimpleStringProperty(m.getAlbum());
			}
		}

		public String getKey() {
			return key.get();
		}
		public Integer getNum() {
			return num.get();
		}
		public String getArtist() {
			return artist.get();
		}
		public String  getTitle() {
			return title.get();
		}
		public String  getAlbum() {
			return album.get();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}

