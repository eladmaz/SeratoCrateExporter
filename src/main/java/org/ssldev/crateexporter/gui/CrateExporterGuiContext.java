package org.ssldev.crateexporter.gui;

/**
 * holds user selected attributes and actions.  to be globally accessed
 */
public class CrateExporterGuiContext {

	private static String loadedCrateName = "";
	private static String loadedContent = "";
	private static String contentAsM3u = "";
	
	public static void loadedCrateName(String name) {
//		Logger.debug(CrateExporterGuiContext.class, "GUI crate name changed to " + name);
		loadedCrateName = name;
	}
	public static void loadedContent(String content) {
//		Logger.debug(CrateExporterGuiContext.class, "GUI content got updated ");
		loadedContent = content;
	}
	
	public static String getCrateName() {
		return CrateExporterGuiContext.loadedCrateName;
	}
	public static String getContentAsText() {
		return CrateExporterGuiContext.loadedContent;
	}
	public static String getContentAsM3u() {
		return CrateExporterGuiContext.contentAsM3u;
	}
	public static void setM3uContent(String contentAsM3u) {
		CrateExporterGuiContext.contentAsM3u = contentAsM3u;
		
	}
	
}
