package org.ssldev.crateexporter.gui;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.ssldev.core.utils.Logger;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

/**
 * models a collapse-able tree of serato crates
 */
public class CrateTreeModel {
	/**
	 * root of the crates dir
	 */
	private final TreeView<Crate> filesTree;
	
	private Map<String, TreeItem<Crate>> cratesByName = new HashMap<>();

	public CrateTreeModel(TreeView<Crate> root) {
		filesTree = root;
	}
	
	/**
	 * creates a {@link TreeItem} for every serato crate encountered
	 * @param dir containing all serato crates
	 */
	public void createTree(File dir) {
	    TreeItem<Crate> root = new TreeItem<>(new Crate(dir));
	    try {
	        File[] files = dir.listFiles((directory, name) -> name.endsWith("crate"));
	        for (File file : files) {
	        	
	        	TreeItem<Crate> node = getOrCreate(file);

	        	if(hasParent(file)) {
	        		TreeItem<Crate> parentCrate = getParentOrCreate(file);
	        		parentCrate.getChildren().add(node);
	        		cratesByName.put(parentCrate.getValue().file.getAbsolutePath(), parentCrate);
	        		
	        		Logger.debug(this, "     file: ["+parentCrate.getValue().file.getName()+"]" + file.getCanonicalPath());
	        	}
	        	else {
	        		Logger.debug(this, "     file: ["+root.getValue().file.getName()+"]" + file.getCanonicalPath());
	        		
	        		root.getChildren().add(node);
	        	}
	        	
	        	cratesByName.put(file.getAbsolutePath(), node);
	        }
	        
	        filesTree.setRoot(root);
	        
	    } catch (IOException e) {
	       e.printStackTrace();
	    }
	} 
	
	public List<Crate> getAllCrates() {
		return cratesByName.values().stream()
				.map(i -> i.getValue())
				.collect(toList());
	}
	
	private TreeItem<Crate> getParentOrCreate(File file) {
		if(!hasParent(file)) {
			throw new IllegalArgumentException(file.getAbsolutePath() + " has no parent crate");
		}
		
		String pathOfParent = getParentPath(file);
		
		return getOrCreate(new File(pathOfParent));
	}

	
	private TreeItem<Crate> getOrCreate(File file) {
		return cratesByName.getOrDefault(file.getAbsolutePath(), new TreeItem<>(new Crate(file)));
	}


	private static String getParentPath(File file) {
		if(!hasParent(file)) {
			return null;
		}
		else {
			String path = file.getAbsolutePath();
			return path.substring(0, path.lastIndexOf("%%")) + ".crate";
		}
	}


	private static boolean hasParent(File file) {
		return file.getAbsolutePath().contains("%%");
	}

	
	/**
	 * does the given crate have children
	 * @param crate to check
	 * @return true if crate is a parent.  false if not or if it could not be found
	 */
	public boolean isParent(Crate crate) {
		TreeItem<Crate> ti = cratesByName.get(crate.file.getAbsolutePath());
		if(null == ti) {
			Logger.warn(this, "could not find ["+crate.name+"] in tree");
		}
		
		return ti == null ? false : ti.getChildren().size() > 0;
	}

	public List<Crate> getChildren(Crate crate) {
		TreeItem<Crate> ti = cratesByName.get(crate.file.getAbsolutePath());
		if(null == ti) {
			Logger.warn(this, "could not find ["+crate.name+"] in tree");
		}
		

		// collect all children..
		Stack<TreeItem<Crate>> items = new Stack<>();
		for(TreeItem<Crate> c : ti.getChildren()) {
			items.push(c);
		}

		
		//...iterate over all children and their sub-children
		List<Crate> res = new ArrayList<>();
		
		while(!items.isEmpty()) {
			TreeItem<Crate> child = items.pop();
			res.add(child.getValue());
			for(TreeItem<Crate> c : child.getChildren()) {
				if(c.getChildren().size() > 0) {
					items.push(c);
				}
				else {
					res.add(c.getValue());
				}
			}
		}
		
		// all children and sub-children
		return res;
	}
	
	
	public static class Crate {
		public final String name;
		public final File file;
		
		public Crate(File file) {
			this.name = file.getName().replaceAll(".*%%", "").replaceAll(".crate$", "");
			this.file = file;
		}
		
		@Override
		public String toString() {
			return name;
		}
	}
}
