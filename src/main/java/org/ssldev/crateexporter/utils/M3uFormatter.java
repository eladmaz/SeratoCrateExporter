package org.ssldev.crateexporter.utils;

import static java.util.stream.Collectors.joining;

import java.util.List;

public class M3uFormatter  implements IFormatter{

	private final static String M3U_FILE_HEADER = "#EXTM3U";
	private final static String M3U_SONG_HEADER = "#EXTINF:";
	
	private final static String LF = System.lineSeparator();
	
	@Override
	public String format(List<String> displayColumns, List<CrateAudioFile> mp3s) {
		
		StringBuilder sb = new StringBuilder(M3U_FILE_HEADER + LF);
		
		sb.append(
			mp3s.stream()
			.map(this::toM3uEntry)
			.filter(m3uEntry -> m3uEntry != null)
			.collect(joining(LF))
			);
		
		return sb.toString();
	}
	
	
	public String toM3uEntry(CrateAudioFile m) {
		if(m == null || m.getF() == null) return null;
		
		return M3U_SONG_HEADER + m.getLengthInSeconds() +", "+m.getArtist()+" - "+m.getTitle()+LF
				+ m.getF().getAbsolutePath();
	}

}
