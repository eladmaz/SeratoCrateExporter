package org.ssldev.crateexporter.utils;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.ssldev.core.utils.Logger;

/**
 * formats MP3s to simple text file format
 */
public class TextFormatter implements IFormatter {
	private static int NUM_COL_WIDTH = 3;
	private static int ARTIST_COL_WIDTH = 40;
	private static int TITLE_COL_WIDTH = 60;
	private static int ALBUM_COL_WIDTH = 60;
	private static String LF = System.lineSeparator();

	@Override
	public String format(List<String> displayColumns, List<CrateAudioFile> mp3s) {
		StringBuilder sb = new StringBuilder();
		
		// this simple implementation will only display #, Artist, album, and Title columns
		sb.append(formatColumns());
		sb.append(LF);
		// add a --- bar
		sb.append(formatBar());
		sb.append(LF);
		// add mp3's
		sb.append(formatMp3s(mp3s));
		
		String content = sb.toString();
		Logger.debug(this, "formatted content: "+LF+content);
		
		return content;
	}
	
	private static String formatColumns() {
		return 
	    String.format("%"+NUM_COL_WIDTH+"s %-"+ARTIST_COL_WIDTH+"s %-"+TITLE_COL_WIDTH+"s %-"+ALBUM_COL_WIDTH+"s", "#", "Artist", "Title", "Album");
	}

	private static String formatBar() {
		int width = NUM_COL_WIDTH + ARTIST_COL_WIDTH + TITLE_COL_WIDTH + ALBUM_COL_WIDTH;
		StringBuilder sb = new StringBuilder();
		while(width-- > 0) {
			sb.append("-");
		}
		return sb.toString();
	}

	private static String formatMp3s(List<CrateAudioFile> mp3s) {
		StringBuilder sb = new StringBuilder();
		final AtomicInteger counter = new AtomicInteger(1);
		mp3s.forEach( mp3 -> {
			sb.append(
					String.format("%"+NUM_COL_WIDTH+"d %-"+ARTIST_COL_WIDTH+"s %-"+TITLE_COL_WIDTH+"s %-"+ALBUM_COL_WIDTH +"s", 
								  counter.getAndIncrement(), 
								  mp3.getArtist(),
								  mp3.getTitle(),
								  mp3.getAlbum()
					))
			  .append(LF);
		});
		
		return sb.toString();
	}

}
