package org.ssldev.crateexporter.utils;

import static org.ssldev.core.utils.TimeUtil.toLocalDate;

import java.io.File;
import java.io.Serializable;

/**
 * represents an mp3 file with ID3 info
 */
public class CrateAudioFile implements Serializable {
	private static final long serialVersionUID = 1L;
	private String title;
	private String artist;
	private String album;
	private String composer;
	private String key;
	private File f;
	private long sizeInBytes;
	private long lastModified;
	private long lengthInSeconds;
	
	/**
	 * set to true if info failed to populate during extraction (e.g. file was not found)
	 */
	private boolean errorOnCreate; 
	
	public String getTitle() {
		return title;
	}
	public CrateAudioFile setTitle(String title) {
		this.title = title;
		return this;
	}
	public String getArtist() {
		return artist;
	}
	public CrateAudioFile setArtist(String artist) {
		this.artist = artist;
		return this;
	}
	public String getAlbum() {
		return album;
	}
	public CrateAudioFile setAlbum(String album) {
		this.album = album;
		return this;
	}
	
	public String getComposer() {
		return composer;
	}
	public CrateAudioFile setComposer(String composer) {
		this.composer = composer;
		return this;
		
	}
	public String getKey() {
		return key;
	}
	public CrateAudioFile setKey(String key) {
		this.key = key;
		return this;
	}
	public File getF() {
		return f;
	}
	public CrateAudioFile setF(File f) {
		this.f = f;
		return this;
	}
	public CrateAudioFile setSize(long l) {
		this.sizeInBytes = l;
		return this;
	}
	
	public long getSize() {
		return sizeInBytes;
	}
	public long getLengthInSeconds() {
		return lengthInSeconds;
	}
	public CrateAudioFile setLengthInSeconds(long lengthInSeconds) {
		this.lengthInSeconds = lengthInSeconds;
		return this;
	}

	public CrateAudioFile setLastModified(long lastModified) {
		this.lastModified = lastModified;
		return this;
	}
	public long getLastModified() {
		return lastModified;
	}
	
	public boolean encounteredErrorOnCreate() {
		return errorOnCreate;
	}
	public CrateAudioFile setErrorOnCreate(boolean error) {
		errorOnCreate = error;
		return this;
	}
	
	@Override
	public String toString() {
		if(errorOnCreate) {
			return "error on: " + getDirPath(f);
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("a:[").append(artist).append("], ");
		sb.append("t:[").append(title).append("], ");
		sb.append("c:[").append(composer).append("], ");
		sb.append("key:[").append(key).append("], ");
		sb.append("size:[").append(sizeInBytes / 1024).append("KB]");
		sb.append("length:[").append(lengthInSeconds).append(" seconds]");
		sb.append("lastMod:[").append(toLocalDate(lastModified)).append("]");
		sb.append("loc:[").append(getDirPath(f));
		return sb.toString();
	}
	private String getDirPath(File n) {
		return n == null ? "null" : n.getParentFile().getAbsolutePath();
	}
	public boolean hasKey() {
		return getKey()!=null && !getKey().equals("empty");
	}
	public boolean hasComposer() {
		return getComposer()!=null && !getComposer().equals("empty");
	}

}
