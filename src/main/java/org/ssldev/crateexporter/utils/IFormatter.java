package org.ssldev.crateexporter.utils;

import java.util.List;

/**
 * Formats columns and MP3s into text blocks
 */
public interface IFormatter {
	
	/**
	 * format the given display columns and mp3s into a string text block
	 * 
	 * @param displayColumns
	 * @param mp3s
	 * @return formatted text block containing columns and mp3s
	 */
	public String format(List<String> displayColumns, List<CrateAudioFile> mp3s);

	public static enum FormatTypes {
		TEXT(".txt"), M3U(".m3u");
		
		/** format file extension */
		public String extension;
		
		private FormatTypes(String extension) {
			this.extension = extension;
		}
	}
}
