package org.ssldev.crateexporter.services;

import static java.io.File.separator;
import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.ssldev.api.messages.CrateConvertRequestMessage;
import org.ssldev.api.messages.CratesConvertRequestMessage;
import org.ssldev.core.messages.WriteToFileMessage;
import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.services.Service;
import org.ssldev.core.utils.Logger;
import org.ssldev.crateexporter.CrateExporterMain;
import org.ssldev.crateexporter.gui.CrateExporterGUI;
import org.ssldev.crateexporter.gui.CrateExporterMainController;
import org.ssldev.crateexporter.gui.CrateTreeModel.Crate;
import org.ssldev.crateexporter.messages.CrateAsM3uMessage;
import org.ssldev.crateexporter.messages.CrateAsTextMessage;
import org.ssldev.crateexporter.messages.ExtractedMp3Message;
import org.ssldev.crateexporter.utils.CrateAudioFile;

import javafx.application.Application;

public class CrateExporterGuiService extends Service{

	/**
	 * service instance.  used as a gateway to the App's model (i.e. event hub)
	 */
	private static CrateExporterGuiService INSTANCE;
	
	/**
	 * GUI controller
	 */
	private CrateExporterMainController controller;
	
	/**
	 * cache crate contents so that they don't have to get re-read
	 */
	private Map<String, CrateContents> crateContentsByCrateName = new HashMap<>();

	
	public CrateExporterGuiService(EventHub hub) {
		super(hub);
		INSTANCE = this;
	}
	
	@Override
	public void init() {
		hub.invokeLater(this::launchGui);
		
		hub.register(CrateAsTextMessage.class, this::onCrateAsText);
		hub.register(ExtractedMp3Message.class, this::onExtractedMp3);
		hub.register(CrateAsM3uMessage.class, this::onCrateAsM3u);
		
	}
	
	public static CrateExporterGuiService instance() {
		return INSTANCE;
	}
	
	public void onSeratoCratesDirSelected(File seratoCratesDir) {
		controller().ifPresent(c -> c.setTreeRoot(seratoCratesDir));
		controller().ifPresent(c -> c.setCratesDirSelected(seratoCratesDir.getAbsolutePath()));
	}
	
	public void onControllerInitialized(CrateExporterMainController instance) {
		this.controller = instance;
		selectDefaultSeratoCrateDirIfAvailable();
	}
	
	private void onCrateAsText(CrateAsTextMessage msg) {
		controller().ifPresent(c -> c.displayCrate(msg.crateName, msg.content, msg.mp3s));
		Logger.debug(this, "putting ["+msg.crateName+"] to map");
		crateContentsByCrateName.put(msg.crateFile.getAbsolutePath(), new CrateContents(msg.crateName, msg.content, msg.mp3s));
	}
	private void onExtractedMp3(ExtractedMp3Message msg) {
		controller().ifPresent(c -> c.updateStatus(msg.getMp3File().getAbsolutePath()));
	}
	private void onCrateAsM3u(CrateAsM3uMessage msg) {
//		crateContentsByCrateName.put(msg.crateName, new CrateContents(msg.crateName, msg.contentAsM3u, msg.mp3s));
		controller().ifPresent(c -> c.setM3uContent(msg.contentAsM3u));
	}
	
	private void launchGui() {
		Thread.currentThread().setName("THREAD - "+CrateExporterGuiService.class+" GUI launch service");
		
		try {
			// note: this is a restricted call. may break with future updates!
			Application.launch(CrateExporterGUI.class, new String[0]);
			// GUI has exited.. tell model to shutdown
			hub.shutdown();
			Logger.info(getClass(), "GUI exit...");
		} catch(IllegalStateException e) {
			Logger.warn(this, "cannot start GUI more than once.");
		}
	}

	// try to display all crates in the default serato subcrate location (e.g. /home/user/Music/_Serato_/Subcrates)
	private void selectDefaultSeratoCrateDirIfAvailable() {
		String userHomeDir = System.getProperty("user.home");
		String defaultSeratoSubcrateDirLocation = userHomeDir + separator + "Music" + separator + "_Serato_" + separator +"Subcrates";
		File seratoSubcrateDir = new File(defaultSeratoSubcrateDirLocation);
		
		if(seratoSubcrateDir.exists() && seratoSubcrateDir.isDirectory()) {
			Logger.info(CrateExporterMain.class, "attempting to read all serato crates at: ["+defaultSeratoSubcrateDirLocation+"]");
			onSeratoCratesDirSelected(seratoSubcrateDir);
		}
		else {
			Logger.info(CrateExporterMain.class, "default serato crate location ["+defaultSeratoSubcrateDirLocation+"] was not found");
		}
	}
	
	// user clicked on a crate in the tree
	public void onCrateSelected(Crate selectedCrate) {
		
		String crateKey = selectedCrate.file.getAbsolutePath() ; //+ ".crate";
		
		if(crateContentsByCrateName.containsKey(crateKey)) {
			Logger.debug(this, crateKey + " already converted");
			
			CrateContents crateContents = crateContentsByCrateName.get(crateKey);
			
			controller().ifPresent(c -> c.displayCrate(crateContents.crateName, crateContents.content, crateContents.tracks));
		}
		else {
			Logger.debug(this, "requesting crate convert for ["+crateKey+"]...");
			controller().ifPresent(c -> c.updateStatus("converting ["+crateKey+"] to text..."));
			hub.add(new CrateConvertRequestMessage(selectedCrate.file, selectedCrate.name));
		}
	}
	
	public void onCratesSelected(Crate parent, List<Crate> children) {
		
		String crateKey = parent.file.getAbsolutePath() ; //+ ".crate";
		if(crateContentsByCrateName.containsKey(crateKey)) {
			Logger.debug(this, crateKey + " already converted");
			
			CrateContents crateContents = crateContentsByCrateName.get(crateKey);
			
			controller().ifPresent(c -> c.displayCrate(crateContents.crateName, crateContents.content, crateContents.tracks));
		}
		else {
			Logger.debug(this, "requesting crate convert for ["+crateKey+"]...");
			controller().ifPresent(c -> c.updateStatus("converting ["+crateKey+"] to text..."));
			hub.add(new CratesConvertRequestMessage(parent.file, parent.name, concat(parent.file, children)));
		}
	}
	
	// concat the given files into one list of files
	private List<File> concat(File parent, List<Crate> children) {
		List<File> res = new ArrayList<>();
		res.add(parent);
		
		List<File> files = children.stream()
								   .map(c -> c.file)
								   .collect(toList());
		res.addAll(files);
		return res;
	}

	/**
	 * write the given contents to the given file path
	 * @param filePath to write to
	 * @param content to write
	 */
	public void writeToFile(String filePath, String content) {
		Logger.info(this, "requesting write crate to ["+filePath+"]..");
		hub.add(new WriteToFileMessage(filePath, content));
	}
	
	private Optional<CrateExporterMainController> controller() {
		return Optional.ofNullable(controller);
	}
	
	protected void doShutdown() {
		// tell model to shutdown
		Logger.debug(this, "told to do shutDown by GUI.. shutting down model");
		CrateExporterMain.shutdown();
	}

	/**
	 * the contents of a crate, including: name, content (in txt form), and ordered list of {@link CrateAudioFile}s 
	 */
	private static class CrateContents {
		final String crateName;
		final String content;
		final  List<CrateAudioFile> tracks;

		public CrateContents(String crateName, String content, List<CrateAudioFile> tracks) {
			this.crateName = crateName;
			this.content = content;
			this.tracks = tracks;
		}
	}



	
}
