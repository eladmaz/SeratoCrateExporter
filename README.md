Serato Crate Exporter (beta v0.3.3)
==================================

This utility provides users with a GUI to decode and export Serato DJ crates into multiple file
formats (powered by [SSL-API](https://gitlab.com/eladmaz/SSL-API)).

please send comments/suggestions via: http://projects.ssldev.org

Dev Notes:
----------
**0.3.3:**
* fixed crash when running on Windows and exporting crates with files that contain unicode chars (issue #5)
* contains latest locally built JaudioTagger (05/01/2021 master branch)

**v0.3.2:**
* fixes issue related to not finding serato subcrates on startup (isue #3)

**v0.3.1:**
* fixed windows missing javafx classes crash (issue #2)
* fixed windows wrong file path assigned to files (issue #2)

**v0.3:** 
* updated to support **java 11**+
* new GUI with collapsible crates
* supports nested crate decoding
* adds supports: mp3, mp4 (mp4 audio, m4a and m4p audio) ogg vorbis, flac and wma, wav, and limited support for Real formats. 

**v0.15:** 
* last version to support **java 8**


Requirements to run Demo jar:
-------------
* JRE (Java Runtime Environment) 11 (e.g. https://adoptopenjdk.net/releases.html)
* only tested on OSX (should work on other platforms/OS's but not tested)

Demo Quick Start (without building project):
------------
1. download `serato-crate-exporter-0.3.3-jar-with-dependencies.jar` from the repo main page
2. open 'terminal'
3. execute the jar (fill in the path to the jar; e.g. /users/bob/downloads/)
   - `java -jar [PATH_TO_JAR]/serato-crate-exporter-0.3.3-jar-with-dependencies.jar`
3. click 'select serato crates' to select location of serato `subcrates` directory file to export (e.g. .../Music/_Serato_/Subcrates/)
4. click 'select output dir' to select directory to export into.
5. select 'export as type' (either txt or m3u)
6. click 'start' to create output file in chosen format
7. click 'open' to open file (uses default OS app; e.g. ITunes for M3U file) 


Quick Start (using maven)
-----------------

1. clone the repo:   
    - `git clone git@gitlab.com:eladmaz/SeratoCrateExporter.git`
2. build the project:   
    - `cd SeratoCrateExporter/`
    - `mvn clean install`
3. start the the app
    - `mvn exec:java`


Known Limitations:
------------------
* crates shared across external and internal drives not supported (only local data is displayed)
* audio on external drives may be missing


Disclaimer:
------------
Serato Crate Exporter is free open source software licensed under the MIT license and is provided 
without warranties of any kind.  

To be safe, please backup your music/Serato library before using this utility!
